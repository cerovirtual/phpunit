<?php

namespace stats\Test;

use stats\Classes\Baseball;

class PrivateMethodTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->instance = new Baseball();
    }

    public function tearDown()
    {
        unset($this->instance);
    }

    public function testPrivateMethod()
    {
        $ops = $this->invokeMethod($this->instance, 'calc_ops', array(0.363, 0.469));
        $expectedResult = 0.363 + 0.469;

        $this->assertEquals($expectedResult, $ops);
    }

    public function invokeMethod(&$object, $methodName, $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
