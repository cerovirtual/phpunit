<?php

namespace stats\Test;

use stats\Classes\Baseball;

class DependencyTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->instance = new Baseball();
    }

    /**
     * tear down method
     */
    public function tearDown()
    {
        unset($this->instance);
    }

    /**
     * because OPS is sum total of on base percentage plus sluggin average, we can use the depends annotation
     */
    public function testSlugging() {
        $slg = $this->instance->calc_slg(389,106,12,4,7);
        $expectedslg = number_format(((106*1)+(12*2)+(4*3)+(7*4)) / 389, 3);
        $this->assertEquals($expectedslg, $slg);

        return $slg;
    }


    public function testOnBasePerc() {
        $obp = $this->instance->calc_obp(389,23,6,7,129);
        $expectedobp =  number_format((129 + 23 + 6 + 7) / 425, 3);
        $this->assertEquals($expectedobp, $obp);

        return $obp;
    }

    /**
     * @depends testSlugging
     * @depends testOnBasePerc
     */
    public function testOps($obp, $slg) {
        $ops = $this->instance->calc_avg($obp,$slg);
        $expectedops = $slg / $obp;

        $this->assertEquals($expectedops,$ops);
    }
}
