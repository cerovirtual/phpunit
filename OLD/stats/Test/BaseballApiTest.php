<?php

namespace stats\Test;

use stats\Classes\BaseballApi;

class BaseballApiTest extends \PHPUnit_Framework_TestCase
{
    public function testMockObject()
    {
        $baseballApi = $this->createMock('stats\Classes\BaseballApi');
        $baseballApi->expects($this->any())
            ->method('submitAtBat')
            ->will($this->returnValue(true));
        $result = $baseballApi->submitAtBat('1', 'bh'); // returns true
        $expected = true;

        $this->assertEquals($expected, $result);
    }

    public function testMockery(){

        $someObj = new BaseballApi();
        $someVal = true;
        $mockeryMock = \Mockery::mock('stats\Classes\BaseballApi');
        $mockeryMock->shouldReceive('submitAtBat')
            ->with('1', 'bh')
            ->once()
            ->andReturn($someVal);

        $this->assertEquals($someVal, $someObj->submitAtBat('1', 'bh'));
    }
}
