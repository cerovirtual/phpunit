<?php

namespace stats\Test;

use stats\Classes\Baseball;

class BaseballTest extends \PHPUnit_Framework_TestCase
{
    public function testCalcAvgEquals()
    {
        $bats = 389;
        $hits = 129;

        $baseball = new Baseball();
        $result =  $baseball->calc_avg($bats, $hits);

        $expectedResult = $hits / $bats;

        return $this->assertEquals($expectedResult, $result);
    }

    public function testCalcHitsAreStrings()
    {
        $bats = 389;
        $hits = "some-string";

        $baseball = new Baseball();
        $result =  $baseball->calc_avg($bats, $hits);

        $expectedResult = 0.000;

        return $this->assertEquals($expectedResult, $result);
    }
}
