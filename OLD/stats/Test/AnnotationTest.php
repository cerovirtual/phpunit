<?php

namespace stats\Test;

use stats\Classes\Baseball;

class AnnotationTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @dataProvider providerCalcArgs
    * @covers Baseball::cal_avg
    * @return float
    * @param int $bats
    * @param int $hits
    */
    public function testCalc($bats, $hits)
    {
        $baseball = new Baseball();

        $result = $baseball->calc_avg($bats, $hits);
        if ( !is_numeric($bats)) {
            $expectedResult = 'Not a number';
        } else {
            $expectedResult = $hits / $bats;
        }

        $this->assertEquals($result, $expectedResult);
    }

    public function providerCalcArgs()
    {
        return array(
            array('389', '129'),
            array('somestring', 129),
            array(389, 'somestring'),
            array('somestring', 'somestring')
        );
    }
}