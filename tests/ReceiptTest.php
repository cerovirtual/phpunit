<?php

namespace TDD\tests;

use TDD\Receipt;

class ReceiptTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Receipt
     */
    public $Receipt;

    public function setUp()
    {
        $this->Receipt = new Receipt();
    }

    public function tearDown()
    {
        unset($this->Receipt);
    }

    public function testTotal()
    {
        $input = [0,2,5,8];
        $coupon = null;
        $expectedResult = 15;
        $result = $this->Receipt
            ->total($input, $coupon);

        $this->assertEquals(
            $expectedResult,
            $result,
            sprintf('When summing the total should equal %s', $expectedResult)
        );
    }

    /**
     * @dataProvider totalProvider
     */
    public function testTotalWithDataProvider($items, $expectedResult, $coupon = null)
    {
        $result = $this->Receipt
            ->total($items, $coupon);

        $this->assertEquals(
            $expectedResult,
            $result,
            sprintf('When summing the total should equal %s', $expectedResult)
        );
    }

    public function totalProvider()
    {
        return [
            'dataprovider-case1' => [[0,2,5,8], 15, null],
            'dataprovider-case2' => [[-1,2,5,8], 14, null],
            'dataprovider-case3' => [[0,2,5,8], 12, 0.20],
            'dataprovider-case4' => [[5,5,20], 27, 0.10],
        ];
    }

    public function testTotalWithCoupon()
    {
        $input = [0,2,5,8];
        $coupon = 0.20;
        $expectedResult = 12;
        $result = $this->Receipt
            ->total($input, $coupon);

        $this->assertEquals(
            $expectedResult,
            $result,
            sprintf('When summing the total should equal %s', $expectedResult)
        );
    }

    public function testTax()
    {
        $inputAmount = 10.00;
        $inputTaxRate = 0.20;
        $expectedResult = 2.00;
        $result = $this->Receipt
            ->tax($inputAmount, $inputTaxRate);

        $this->assertEquals(
            $expectedResult,
            $result,
            sprintf('The tax amount should equal %s', $expectedResult)
        );
    }

    public function testPostTaxTotal()
    {
        $items = [1,2,5,8];
        $tax = 0.20;
        $coupon = null;

        $Receipt = $this->getMockBuilder('TDD\Receipt')
            ->setMethods(['total','tax'])
            ->getMock();

        $Receipt->expects($this->once())
            ->method('total')
            ->with($items, $coupon)
            ->will($this->returnValue(10.00));

        $Receipt->expects($this->once())
            ->method('tax')
            ->with(10, $tax)
            ->will($this->returnValue(2.00));

        $result = $Receipt->postTaxTotal(
            [1,2,5,8],
            0.20,
            null
        );
        $expectedResult = 12.00;

        $this->assertEquals(
            $expectedResult,
            $result,
            sprintf('The post tax total amount should equal %s', $expectedResult)
        );
    }

    public function testTotalException()
    {
        $input = [0,2,5,8];
        $coupon = 1.20;
        $this->expectException('BadMethodCallException');
        $this->Receipt
            ->total($input, $coupon);
    }
}
