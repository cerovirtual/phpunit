<?php

namespace TDD;

class Receipt
{
    public function total(array $item = [], $coupon = null)
    {
        if ($coupon > 1.00) {
            throw new \BadMethodCallException('Coupon percentage should not be greater than 1.00');
        }

        $total = array_sum($item);

        if ( !is_null($coupon)) {
            $total = $total - ($total * $coupon);
        }

        return $total;
    }

    public function tax($inputAmount, $inputTaxRate)
    {
        return $inputAmount * $inputTaxRate;
    }

    public function postTaxTotal($items, $tax, $coupon = null)
    {
        $subTotal = $this->total($items, $coupon);
        $tax = $this->tax($subTotal, $tax);

        return $subTotal + $tax;
    }
}
